#include <iostream>
#include <fstream>
#include "Network/NetworkManager.h"
#include "Serialization/ISerializable.h"

using namespace std;

class Base : public ISerializable {
public:
	float f;

	SerializedData Serialize() {
		SerializedData data;
		data.Push(f);
		return data;
	}

	void Deserialize(SerializedData &data) {
		data.Pop(f);
	}
};

class Derived : public Base {
public:
	double d;

	SerializedData Serialize() {
		SerializedData data;
		data.Push(Base::Serialize());
		data.Push(d);
		return data;
	}

	void Deserialize(SerializedData &data) {
		__super::Deserialize(data);
		data.Pop(d);
	}
};

int main() {
	Base b, b2;
	Derived d, d2;
	b.f = 321.0f;
	d.d = 123.0;

	SerializedData data;
	data.Push(b.Serialize());
	data.Push(d.Serialize());
	data.SaveToFile("data");

	SerializedData data2("data");
	b2.Deserialize(data2);
	d2.Deserialize(data2);

	cout << b.f << " == " << b2.f << endl;
	cout << d.d << " == " << d2.d << endl;

	system("pause");
	return 0;
}