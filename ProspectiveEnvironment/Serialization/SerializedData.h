#pragma once

#include <cinttypes>
#include <vector>
#include <string>

using std::string;
using std::vector;

class SerializedData {
	vector<uint8_t> *mData;
	size_t mWritePointer = 0;
	size_t mReadPointer = 0;

public:
	SerializedData();
	SerializedData(uint8_t *data, size_t dataLen);
	SerializedData(string filename);

	void AddData(uint8_t *data, size_t dataLen);
	bool LoadFromFile(string filename);
	bool SaveToFile(string filename);

	vector<uint8_t> *GetByteArray();
	size_t GetSize();

	template<typename T> void Push(T &object) {
		mData->resize(mData->size() + sizeof(object));
		memcpy(&mData->data()[0] + mWritePointer, &object, sizeof(T));
		mWritePointer += sizeof(T);
	}

	void Push(SerializedData object) {
		mData->insert(mData->begin() + mWritePointer, object.GetByteArray()->begin(), object.GetByteArray()->end());
		mWritePointer += object.GetSize();
	}

	void Push(string &object) {
		size_t size = object.length();
		Push(size);
		mData->resize(mData->size() + object.length());
		memcpy(&mData->data()[0] + mWritePointer, &object[0], object.length());
		mWritePointer += object.length();
	}

	template<typename T> void Pop(T &object) {
		memcpy(&object, &mData->data()[0] + mReadPointer, sizeof(T));
		mReadPointer += sizeof(T);
	}

	void Pop(string &object) {
		size_t size;
		Pop(size);
		object.clear();
		object.resize(size);
		memcpy(&object[0], &mData->data()[0] + mReadPointer, size);
		mReadPointer += size;
	}
};