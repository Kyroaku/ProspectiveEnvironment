#include "SerializedData.h"

#include <fstream>

using std::fstream;

SerializedData::SerializedData() {
	mData = new vector<uint8_t>();
}

SerializedData::SerializedData(uint8_t *data, size_t dataLen) {
	mData = new vector<uint8_t>(data, &data[dataLen]);
}

SerializedData::SerializedData(string filename)
	: SerializedData()
{
	LoadFromFile(filename);
}

void SerializedData::AddData(uint8_t * data, size_t dataLen)
{
	mData->insert(mData->end(), data, data + dataLen);
}

bool SerializedData::LoadFromFile(string filename)
{
	fstream file;
	file.open(filename, fstream::in); 
	if (!file.is_open())
		return false;

	char buffer[1024];
	while (!file.eof()) {
		file.read(buffer, 1024);
		this->AddData((uint8_t*)buffer, (size_t)file.gcount());
	}
	return true;
}

bool SerializedData::SaveToFile(string filename)
{
	fstream file;
	file.open(filename, fstream::out);
	if (!file.is_open())
		return false;

	size_t len = GetSize();
	size_t pos = (size_t)file.tellg();
	while (len > 0) {
		file.write((const char*)&GetByteArray()->front(), GetSize());
		size_t tellg = (size_t)file.tellg();
		len -= tellg;
		pos = tellg;
	}
	return true;
}

vector<uint8_t> *SerializedData::GetByteArray() {
	return mData;
}

size_t SerializedData::GetSize() {
	return (mWritePointer - mReadPointer);
}