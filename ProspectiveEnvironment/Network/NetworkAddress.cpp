#include "NetworkAddress.h"

NetworkAddress::NetworkAddress(string ip, uint16_t port)
	: mIp(ip)
	, mPort(port) {

}

BluetoothAddress::BluetoothAddress(string uuid, uint64_t deviceAddr)
	: mUuid(uuid)
	, mDeviceAddress(deviceAddr) {

}