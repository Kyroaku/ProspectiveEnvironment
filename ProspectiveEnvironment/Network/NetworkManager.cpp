#include "NetworkManager.h"

#include <string>

NetworkManager::NetworkManager() {
}

NetworkManager::~NetworkManager() {
	WSACleanup();
}

NetworkManager &NetworkManager::GetInstance() {
	static NetworkManager manager;
	return manager;
}

bool NetworkManager::Init() {
	WSADATA wsa;
	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0) {
		return false;
	}
	return true;
}