#pragma once

#include <WinSock2.h>
#include <WS2tcpip.h>
#include <ws2bth.h>
#include <string>
#include <memory>

#include "NetworkAddress.h"

using std::string;
using std::shared_ptr;

class Socket {
public:
	enum EProtocol {
		eTcpProtocol,
		eUdpProtocol,
		eBluetoothProtocol
	};

private:
	int mMaxClients = 1;

protected:
	SOCKET mSocket;
	sockaddr_in mAddrInfo;

public:
	Socket(SOCKET sock = NULL);
	Socket(EProtocol protocol);
	virtual ~Socket();

	bool Create(EProtocol protocol);
	bool Bind(NetworkAddress address);
	bool Listen();
	bool OpenServer(string ip, uint16_t port);
	bool Connect(NetworkAddress addr);
	bool Connect(BluetoothAddress addr);
	shared_ptr<Socket> Accept();

	void SetMaxClients(int maxClients);

	void SetBroadcastEnabled(bool b);
	void SetAddressReusable(bool b);

	string GetIp();
	uint16_t GetPort();
	SOCKET GetSocket();

	void SetSocket(SOCKET sock);
	void SetAddrInfo(sockaddr_in &addr);

	bool Send(void *data, int size);
	int Receive(void *data, int buf_size, int data_size = 0);

	int SendTo(NetworkAddress address, void *data, int size);
	int ReceiveFrom(void *data, int size, NetworkAddress *address = 0);

	bool IsConnected();
	void Close();
};