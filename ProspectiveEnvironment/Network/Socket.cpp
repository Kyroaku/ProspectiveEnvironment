#include "Socket.h"

#pragma comment(lib, "Rpcrt4")

Socket::Socket(SOCKET sock /* = NULL */)
	: mSocket(sock) {
}

Socket::Socket(EProtocol protocol) {
	Create(protocol);
}

Socket::~Socket() {
	Close();
}

bool Socket::Create(EProtocol protocol)
{
	switch (protocol) {
	case eTcpProtocol:
		mSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		break;

	case eUdpProtocol:
		mSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		break;

	case eBluetoothProtocol:
		mSocket = socket(AF_BTH, SOCK_STREAM, BTHPROTO_RFCOMM);
		break;
	}
	if (mSocket == INVALID_SOCKET)
		return false;
	else
		return true;
}

bool Socket::Bind(NetworkAddress address) {
	sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(address.mPort);
	InetPton(addr.sin_family, address.mIp.c_str(), &addr.sin_addr.s_addr);

	if (bind(mSocket, (sockaddr*)&addr, sizeof(addr)) == SOCKET_ERROR)
		return false;
	else
		return true;
}

bool Socket::Listen()
{
	if (listen(mSocket, mMaxClients) == SOCKET_ERROR) {
		return false;
	}
	return true;
}

bool Socket::OpenServer(string ip, uint16_t port) {
	if (!Bind(NetworkAddress(ip, port)))
		return false;

	if (!Listen())
		return false;

	return true;
}

bool Socket::Connect(NetworkAddress addr) {
	mAddrInfo.sin_family = AF_INET;
	mAddrInfo.sin_port = htons(addr.mPort);
	InetPton(AF_INET, addr.mIp.c_str(), &mAddrInfo.sin_addr.s_addr);

	if (connect(mSocket, (const sockaddr*)&mAddrInfo, sizeof(mAddrInfo)) == SOCKET_ERROR) {
		this->Close();
		return false;
	}

	return true;
}

bool Socket::Connect(BluetoothAddress addr)
{
	UUID uuid;
	UuidFromString((unsigned char*)addr.mUuid.c_str(), &uuid);

	SOCKADDR_BTH sab;
	memset(&sab, 0, sizeof(sab));
	sab.addressFamily = AF_BTH;
	sab.btAddr = addr.mDeviceAddress;
	sab.serviceClassId = uuid;

	if (connect(mSocket, (sockaddr*)&sab, sizeof(sab)) == SOCKET_ERROR)
		return false;

	return true;
}

shared_ptr<Socket> Socket::Accept() {
	shared_ptr<Socket> client(new Socket);
	SOCKET sock;
	sockaddr_in addr;
	int len = sizeof(addr);
	sock = accept(mSocket, (sockaddr*)&addr, &len);
	client->SetSocket(sock);
	client->SetAddrInfo(addr);
	return client;
}

void Socket::SetMaxClients(int maxClients) {
	mMaxClients = maxClients;
}

void Socket::SetBroadcastEnabled(bool b) {
	int val = b ? 1 : 0;
	setsockopt(mSocket, SOL_SOCKET, SO_BROADCAST, (const char*)&val, sizeof(val));
}

void Socket::SetAddressReusable(bool b) {
	int val = b ? 1 : 0;
	setsockopt(mSocket, SOL_SOCKET, SO_REUSEADDR, (const char*)&val, sizeof(val));
}

string Socket::GetIp() {
	char buff[16];
	InetNtop(mAddrInfo.sin_family, &mAddrInfo.sin_addr, buff, sizeof(buff));
	return string(buff);
}

uint16_t Socket::GetPort() {
	return ntohs(mAddrInfo.sin_port);
}

SOCKET Socket::GetSocket() {
	return mSocket;
}

void Socket::SetSocket(SOCKET sock) {
	mSocket = sock;
}

void Socket::SetAddrInfo(sockaddr_in &addr) {
	mAddrInfo = addr;
}

bool Socket::Send(void *data, int size) {
	char *buffer = (char*)data;
	int sent = 0;
	while (size > 0) {
		sent = send(mSocket, buffer, size, 0);
		if (sent == SOCKET_ERROR) {
			return false;
		}
		buffer += sent;
		size -= sent;
	}
	return true;
}

int Socket::Receive(void *data, int buf_size, int data_size /* = 0 */) {
	int received = 0;
	int totalReceived = 0;
	char *buffer = (char*)data;
	do {
		received = recv(mSocket, buffer, buf_size, 0);
		if (received <= 0) {
			return received;
		}
		buffer += received;
		totalReceived += received;
	} while (data_size > totalReceived && buf_size > totalReceived);
	return totalReceived;
}

int Socket::SendTo(NetworkAddress address, void *data, int size) {
	sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(address.mPort);
	InetPton(addr.sin_family, address.mIp.c_str(), &addr.sin_addr.s_addr);

	return sendto(mSocket, (char*)data, size, 0, (sockaddr*)&addr, sizeof(addr));
}

int Socket::ReceiveFrom(void *data, int size, NetworkAddress *address /* = 0 */) {
	sockaddr_in addr;
	int addrLen = sizeof(addr);
	int recvLen = recvfrom(mSocket, (char*)data, size, 0, (sockaddr*)&addr, &addrLen);
	if (address) {
		char ipbuff[16];
		InetNtop(addr.sin_family, &addr.sin_addr, ipbuff, sizeof(ipbuff));
		address->mIp = string(ipbuff);
		address->mPort = ntohs(addr.sin_port);
	}
	return recvLen;
}

bool Socket::IsConnected() {
	return (mSocket != SOCKET_ERROR && mSocket != INVALID_SOCKET && mSocket != NULL);
}

void Socket::Close() {
	if (mSocket) {
		shutdown(mSocket, SD_BOTH);
		closesocket(mSocket);
		mSocket = NULL;
	}
}